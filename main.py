from app import app, data_file
from argparse import ArgumentParser
import json

# Lets add in some argument parsing
args = ArgumentParser()
args.add_argument("--data")
cli_args = args.parse_args()


if __name__ == "__main__":
    # Only for debugging while developing
    app.run(host="0.0.0.0", debug=True, port=80)

#!/bin/bash
app="docker.test"
docker stop docker.test
docker build -t ${app} .
docker run --rm -d -p 56733:80 \
  --name=${app} \
  ${app}

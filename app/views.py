from app import app, api, data_file
from flask_restful import Resource


class Version(Resource):
    def get(self):
        return {"Mock-API-version": "0.0.1"}


class Database(Resource):
    def get(self):
        return data_file


api.add_resource(Version, "/")
api.add_resource(Database, "/db")

from flask import Flask
from flask_restful import Resource, Api
import json

app = Flask(__name__)
api = Api(app)

try:
    json_data = open("datafile/data.json", "r")
    data_file = json.load(json_data)
    print("Successfully Opened Data file")
except IOError:
    print("Unable to open file")

from app import views
from app import cloudflare_api

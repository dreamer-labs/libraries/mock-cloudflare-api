from flask_restful import Resource, reqparse
from app import api, data_file
from flask import jsonify, request
import json

V4_URL = "/client/v4"


class Zones(Resource):
    def get(self):
        response = {}
        parser = reqparse.RequestParser()
        parser.add_argument("name", type=str, location="values")
        args = parser.parse_args()

        if args.name is None:
            return data_file["zones"]

        try:
            lists = next(
                item for item in data_file["zones"] if item["name"] == args.name
            )
        except StopIteration:
            return {}

        response["result"] = []
        response["result"].append(lists)
        response["error"] = []
        response["messages"] = []
        response["result_info"] = {}
        response["result_info"]["page"] = 1
        response["result_info"]["per_page"] = 20
        response["result_info"]["count"] = 1
        response["result_info"]["total_pages"] = 1
        response["result_info"]["total_count"] = 200
        response["success"] = True

        return response


class DNSRecords(Resource):
    def get(self, zone_id):
        response = {}
        parser = reqparse.RequestParser()
        parser.add_argument("content", type=str, location="values")
        parser.add_argument("type", type=str, location="values")
        parser.add_argument("name", type=str, location="values")
        args = parser.parse_args()

        try:
            records = next(
                item
                for item in data_file["dns_records"]
                if (item["zone_id"] == zone_id
                    and item["name"] == args.name
                    and item["type"] == args.type
                    and item["content"] == args.content)
            )
        except StopIteration:
            return response

        response["result"] = []
        response["result"].append(records)
        response["error"] = []
        response["messages"] = []
        response["result_info"] = {}
        response["result_info"]["page"] = 1
        response["result_info"]["per_page"] = 20
        response["result_info"]["count"] = 1
        response["result_info"]["total_pages"] = 1
        response["result_info"]["total_count"] = 1
        response["success"] = True

        print(response)
        return response


class DNSRecordsUpdate(Resource):
    def put(self, zone_id, dns_record_id):
        parser = reqparse.RequestParser()
        parser.add_argument("content", type=str, location="json")
        parser.add_argument("type", type=str, location="json")
        parser.add_argument("name", type=str, location="json")
        parser.add_argument("ttl", type=int, location="json")
        parser.add_argument("proxied", type=bool, location="json")
        args = parser.parse_args()
        response = {}

        try:
            records = next(
                item
                for item in data_file["dns_records"]
                if (item["zone_id"] == zone_id and item["id"] == dns_record_id)
            )
        except StopIteration:
            return response

        if args["name"] is not None:
            records["name"] = args["name"]
        if args["content"] is not None:
            records["content"] = args.content
        if args["ttl"] is not None:
            records["ttl"] = args.ttl
        if args["type"] is not None:
            records["type"] = args.type
        if args["proxied"] is not None:
            records["proxied"] = args.proxied

        response["result"] = []
        response["result"].append(records)
        response["error"] = []
        response["messages"] = []
        response["result_info"] = {}
        response["result_info"]["page"] = 1
        response["result_info"]["per_page"] = 20
        response["result_info"]["count"] = 1
        response["result_info"]["total_pages"] = 1
        response["result_info"]["total_count"] = 1
        response["success"] = True

        return response


api.add_resource(Zones, "{}".format(V4_URL + "/zones"))
api.add_resource(DNSRecords, "{}/zones/<string:zone_id>/dns_records".format(V4_URL))
api.add_resource(
    DNSRecordsUpdate,
    "{}/zones/<string:zone_id>/dns_records/<string:dns_record_id>".format(V4_URL),
)
